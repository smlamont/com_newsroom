newsroom
===========
City Newsroom Application. 



Configuration
=============

The application can be configured to run in various modes:

    comNewsroom.render({
    items_per_page: 6,
    list_row_class: 'two-per-row',        //blank is one per row
    max_pages: 1,
    more_news_link: 'http://google.ca',   //only useful if max_pages = 1
    initial_topic: "Health",
    initial_type: "Store",
    show_filters : false
    }
  ); //render the application


For embedded apps:
------------------
This app can be embedded on COM Wordpress pages with:

`[rew_app app="newsroom"][/rew_app]`

