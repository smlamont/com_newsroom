/*
 The gulpfile for WCM-embedded web apps using the rewindJS library combined with yarn and gulp.
 You shouldn't have to change this file much, but you could if you wanted.
 Note that this file uses es6 syntax.
 http://gulpjs.com/
 */

const gulp = require('gulp');
const rewindJS = require('./node_modules/@rewindJS_components/rewindjS/gulp_helper');
const pkg = require('./package.json');


//You should pass options to the createTasks method below
let options = {
  pkg, //pass in the contents of package.json

  //How this app should be embedded in the simulator. Options are: 'full', 'left', or 'right'
  //This is optional, and the default value is 'full'
  //This only applies to apps where isEmbedded is true
  embedArea: 'left',

  //If you want to add vars to the preprocessor context, include this option
  //You can add environment-specific vars or general vars
  preprocessorContext: {
    local: {
	  //SITE_URL: 'https://contrib-qa.cms.mississauga.ca',
	  //SITE_URL: 'http://35.183.5.159',
	  //SITE_URL: 'http://cmsapache.local',
	  SITE_URL: '',
    },
    dev: {
	   SITE_URL: '',
    },
    qa: {
	   SITE_URL: '',
    },
    prod: {
	  SITE_URL: '',
    },
    SOME_OTHER_ENV: 'this var will be in the context of any environment'
  },

  //If you want to override the environment that the build process uses, specify it here
  //Valid values are: 'local', 'dev', 'qa', 'prod'
  //If you omit this (which you probably should), then the environment will be:
  //'local' when running or building on your machine
  //'dev' when calling gulp deploy:dev
  //'qa' when calling gulp deploy:qa
  //'prod' when calling gulp deploy:prod
  environmentOverride: null,

  //By default, standalone apps are built to deploy to /webapps/appname
  //BUT you can use this option to override that value. ex: /webapps/work/decom/appname
  //IMPORTANT!! this value should NOT end with a forward slash
  //This option has no effect on embedded apps, which are always deployed to /resources/app_name
  //The value used here will also be used for the SRC_PATH preprocessor var
  deploymentPath: '/dev-webapps/com_newsroom'
};

//This creates several gulp tasks to use during development:
//default, clean, build, build_with_simulator, run, deploy:dev, deploy:qa, deploy:prod
rewindJS.createTasks(gulp, options);
rewindJS.vars.compress = false;

// });
gulp.task('_data', () => {
   let myDataPath = '/data'; //On S3, this will be something like /data/division/my_app
                             //On WP, this will be something different
   return gulp.src(['src/data/**/*']).pipe(gulp.dest('dist' + myDataPath));
});

