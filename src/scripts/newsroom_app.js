/**
 Author: Steve Lamont
 Company: Rewind Consulting Inc

 Notes: Topic and Categories are the same..
 */

class NewsroomApp extends (window['rewindJSApp'] || window['rewindJS_app']) {
  constructor(name, options) {
    super(name, options);
    this.SITE_URL = "/* @echo SITE_URL */";
    this.config = {};
    this.data_feed = [];
    this.publish_date_format = 'YYYY-MM-DD HH:mm:ss';
    this.display_publish_date_format = 'MMM DD, YYYY';
    this.current_page = 1;
    this.total_pages = 10;
    this.total_posts = 0;
    this.default_range_startDate = moment('20180101');
    this.default_range_endDate = moment();
    this.$container = $('#com_newsroom_container');
    this.app_router = null;
    this.categoryList = [];
    this.categories = {};
    this.newsTypes = {};
    this.newsTypeList = [];
    this.initialized = false;

    //We determine the Site API by looking at the link tags, unless it is overwritten in the gulp setting
    // We also update the rss feed links

    if (this.SITE_URL === "") {
      this.SITE_API_BASE = $("link[rel='https://api.w.org/']").attr("href");
      this.SITE_URL = this.SITE_API_BASE.replace("/wp-json/","");
      $(".rss-feed-link").each((i, item) => {
        $(item).attr("href", this.SITE_URL + "/news/feed/");
      });
    } else {
      this.SITE_API_BASE = this.SITE_URL + "/wp-json/";

    }
  }

  render(config) {
    $.extend(this.config, {
      items_per_page: 10,
      max_pages: 100,
      list_row_class: "",
      show_filters: true,
    }, config || {});
    //@if !IS_EMBEDDED
    super.render(); //this function only exists in rewindJS_app
    //@endif
    this.handlebarsSetup();
    this.setUpApp();
  }

  getNewHashValueStr(hObj) {
    let hash = rewindJSUtils.getHashValues();
    hash = rewindJSUtils.extendAndCleanHashValues(hash,hObj);
    //hash = $.extend({}, hash, hObj);
    let hashStr = rewindJSUtils.getHashValueStr(hash);
    this.setAppliedFilters(hash);
    return hashStr;
  }

  parseDateParm(str) {
    let dates = str.split('_');
    return {'from': dates[0], 'to': dates[1]};
  }

  setSearchClearer(val) {
    if (val) {
      $(".search-clear").removeClass("hide");
    } else {
      $(".search-clear").addClass("hide");
    }
  }

  collapseFilterPanel() {
    let fcPanel = $('#filter-controls');
    let fcLink = $('a[href="#filter-controls"]');
    fcPanel.removeClass('in');
    fcPanel.attr('aria-expanded', false);
    fcLink.addClass('collapsed');
    fcLink.attr('aria-expanded', false);
  }


  toggleLoader(on) {
    if (on) {
      // noinspection Annotator
      this.$container.find('#form_loader').removeClass('hide').attr('aria-hidden', false);
    } else {
      // noinspection Annotator
      this.$container.find('#form_loader').addClass('hide').attr('aria-hidden', true);
    }
  }

  handlebarsSetup() {
    Handlebars.registerHelper('formatDate', (dt) => {
      if (dt) {
        return moment(dt).format(this.display_publish_date_format)
      } else {
        return "";
      }
    })
  }

  //We will always order all data by spotlight, then publish date

  getFilterparms() {
    let hash = rewindJSUtils.getHashValues();
    let filterParms = "";
    if (hash['type']) {
      filterParms += "&filter[meta_query][0][key]=news_type&filter[meta_query][0][value]=" + hash['type'];
    }
    if (hash['topic']) {
      filterParms += "&categories=" + hash['topic'];
    }
    //&after=2018-04-16T18:00:00&before=2018-04-16T19:00:00
    if (hash['dates']) {
      let dts = this.parseDateParm(hash['dates']);
      filterParms += "&after=" + moment(dts.from, "YYYY-MM-DD").startOf('day').format();
      filterParms += "&before=" + moment(dts.to, "YYYY-MM-DD").endOf('day').format();
    }
    if (hash['search']) {
      filterParms += "&search=" + hash['search'];
    }
    return filterParms;
  }

  renderPageOfData() {

    this.toggleLoader(true);
    let orderBy = "&filter[orderby][meta_value_num]=DESC&filter[meta_key]=spotlight&filter[orderby][date]=DESC";
    let filterParms = this.getFilterparms();
    let pagingParms = "?per_page=" + this.config.items_per_page + "&page=" + this.current_page;
    return $.ajax({
      type: 'GET',
      url: this.SITE_API_BASE + "wp/v2/news" + pagingParms + filterParms + orderBy,
      dataType: 'json',
      contentType: "application/json",
      success: (data, status, request) => {
        this.total_posts = parseInt(request.getResponseHeader('x-wp-total'));
        this.total_pages = parseInt(request.getResponseHeader('x-wp-totalpages'), 10);
        if (this.total_pages === 0) this.total_pages = 1;
        this.data_feed = data;
        this.renderPage();
        this.toggleLoader(false);
      },
      error: () => {
        bootbox.alert("Application failed to load. Please try again later.");
        this.toggleLoader(false);
      }
    });

  }

  renderPagination() {
    let $pagingWidget = $('#numberPageSelection');
    $pagingWidget.html("");
    //This redraws the paging widget
    $pagingWidget.bootpag({
      total: (this.config.max_pages > this.total_pages ? this.total_pages : this.config.max_pages)
    });

    //set bootPag default rendering too match COM standards
    $pagingWidget.find("li").filter(".next,.prev").not(".next.disabled,.prev.disabled").each(function (i, item) {
      $(item).html("<span>" + item.innerHTML + "</span>");
    });
    $pagingWidget.find("li").filter(".next.disabled,.prev.disabled").each(function (i, item) {
      $(item).html("<span aria-hidden='true'>" + $(item).find("a span").html() + "</span>");
    });
    //set bootPag default rendering too match COM standards when changing pages
    $pagingWidget.find("li").not(".next,.prev,.active").each(function (i, item) {
      $(item).html('<a href="javascript:void(0);"><span>' + $(item).data("lp") + '</span></a>');
    });
    $pagingWidget.find("li").filter(".active").each(function (i, item) {
      $(item).html("<span>" + item.innerText + "<span class='sr-only'>(current)</span></span>");
    });
  }

  renderPage() {

    if (this.data_feed.length === 0) {
      this.data_feed.push({"title": {"rendered": "No Data Found"}})
    }
    let start = (this.current_page - 1) * this.config.items_per_page + 1;
    if (start > this.total_posts) {
      start = this.total_posts;
    }
    let end = (this.current_page) * this.config.items_per_page;
    if (end > this.total_posts) {
      end = this.total_posts
    }
    let viewStats = {start: start, end: end, totalPosts: this.total_posts};
    let itemContent = {items: this.data_feed, stats: viewStats, row_class: this.config.list_row_class};
    let html = com_newsroom.templates.listing(itemContent);
    $("#newsroom_list_view").html(html);

    if (this.config.max_pages > 1) {
      this.renderPagination();
    }
  }

  //This is turning out to be specific for each parameter
  setAppliedFilters(hObj) {
    let appliedFilters = [];
    let dateFormat = this.display_publish_date_format;
    $.each(Object.keys(hObj), (i, key) => {
      switch (key) {
        case 'dates' : {
          let dts = this.parseDateParm(hObj[key]);
          let dDates = moment(dts.from, "YYYY-MM-DD").format(dateFormat) + " - " + moment(dts.to, "YYYY-MM-DD").format(dateFormat);
          appliedFilters.push({"key": key, "value": dDates});
          break;
        }
        // case 'search' : {
        //   break; //ignore this option
        //}
        case 'topic' : {
          appliedFilters.push({"key": key, "value": this.categories[hObj[key]]});
          break; //ignire this option
        }
        case 'type' : {
          appliedFilters.push({"key": key, "value": this.newsTypes[hObj[key]]});
          break; //ignire this option
        }
        default: {
          appliedFilters.push({"key": key, "value": hObj[key]});
        }
      }
    });
    let html = com_newsroom.templates.applied_filters({"appliedFilters": appliedFilters});
    $(".applied-filters").html(html);
    $(".applied-filter-count").html('(' + appliedFilters.length + ')');
  }

  setUpEvents() {

    let $main = $("#com_newsroom_container");
    let topicFilter = $("#filter-topic");
    topicFilter.on("change", (evt) => {
      let newHash = this.getNewHashValueStr({"topic": evt.target.value});
      this.app_router.navigate(newHash, {trigger: true, replace: true});
    });
    let typeFilter = $("#filter-type");
    typeFilter.on("change", (evt) => {
      let newHash = this.getNewHashValueStr({"type": evt.target.value});
      this.app_router.navigate(newHash, {trigger: true, replace: true});
    });
    let dp = $('#filter-date');
    dp.on('show.daterangepicker', function (evt, picker) {
      $($(picker)[0]).focus();
    });
    $('.daterangepicker').attr('aria-hidden', true);

    dp.on('apply.daterangepicker', (evt, picker) => {
      //console.log(picker.startDate.format('YYYY-MM-DD'));
      //console.log(picker.endDate.format('YYYY-MM-DD'));
      let newHash = this.getNewHashValueStr({
        "dates": picker.startDate.format('YYYY-MM-DD') + "_" + picker.endDate.format('YYYY-MM-DD')
      });
      this.app_router.navigate(newHash, {trigger: true, replace: true});
    });

    $main.on("click", "button[data-input=com_news_search]", () => {
      let val = $("#com_news_search").val();
      this.setSearchClearer(val);
      let newHash = this.getNewHashValueStr({"search": val});
      this.app_router.navigate(newHash, {trigger: true, replace: true});
    });
    $("#com_news_search").on("keydown", (evt) => {
      let KeyCode = evt.which || evt.keyCode || 0;
      if (KeyCode === 13 || KeyCode === 9) {
        this.setSearchClearer(evt.target.value);
        let newHash = this.getNewHashValueStr({"search": evt.target.value});
        this.app_router.navigate(newHash, {trigger: true, replace: true});
      }
    });
    $(".search-clear").on("click", () => {
      $("#com_news_search").val("");
    });

    $main.on("click", ".app-filters-reset button", () => {
      let hashVals = rewindJSUtils.getHashValues();
      if (!$.isEmptyObject(hashVals)) {
        let obj = {"type": "", "topic": "", "dates": "", "search": ""};
        let newHash = this.getNewHashValueStr(obj);
        this.resetFilters();
        this.app_router.navigate(newHash, {trigger: true, replace: true});
      }
    });

    $main.on("click", ".applied-filters button", (evt) => {
      let key = $(evt.currentTarget).find("span").data("hashkey");
      let obj = {};
      obj[key] = "";
      let newHash = this.getNewHashValueStr(obj);
      this.app_router.navigate(newHash, {trigger: true, replace: true});
    });
  }

  getNewsTypes() {

    return $.ajax({
      type: 'GET',
      //url: "/* @echo NEWS_TYPE_URL*/",
      url: this.SITE_API_BASE + "rewind/v1/acf_select_choices/news_type",
      dataType: 'json',
      contentType: "application/json",
      success: (data, status, request) => {
        this.newsTypes = {};
        this.newsTypeList = [];
        this.newsTypeList.push({"text": "Select a type…", "value": ""});
        $.each(data.results, (i, type) => {
          this.newsTypeList.push({"text": type.text, "value": type.id});
          this.newsTypes[type.id] = type.text;
        });
      },
      error: () => {
        bootbox.alert("Application failed to load. Please try again later.");
      }
    });

  }

  getCategories() {

    this.categoryList = [];
    this.categories = {};
    let pagingParms = "?per_page=100";
    return $.ajax({
      type: 'GET',
      url: this.SITE_API_BASE + "wp/v2/categories" + pagingParms,
      dataType: 'json',
      contentType: "text/plain",
      success: (data, status, request) => {
        this.total_cats = request.getResponseHeader('x-wp-total');

        this.categoryList.push({"text": "Select a topic…", "value": ""});
        data.sort(rewindJSUtils.sortJSONData('name'));
        $.each(data, (i, cat) => {
          this.categoryList.push({"text": cat.name, "value": cat.id});
          this.categories[cat.id] = cat.name;
        });

      },
      error: (a, b, c) => {
        bootbox.alert("Application failed to load. Please try again later.");
      }
    });
  }

  setUpPagination() {
    if (this.config.max_pages > 1) {
      $('#numberPageSelection').bootpag({
        total: this.total_pages,
        page: 1,
        maxVisible: 10,
        leaps: false,
        firstLastUse: false,
        next: '<span>»</span>',
        prev: '<span>«</span>',
        wrapClass: 'pagination',
        activeClass: 'active',
        disabledClass: 'disabled',
      }).on("page", (event, num) => {
        this.current_page = num;
        this.renderPageOfData();
      });
    } else {
      $('#numberPageSelection').html('<ul class="pagination_more_news"><li  class="more"><span><a href="' + this.config.more_news_link + '"><span>View all news <span class="glyphicon glyphicon-chevron-right"></span></span></a></span></li></ul>').addClass("no-paging");
    }
  }

  setUpFilterless() {
    $(".form-filter-block").addClass('hidden');
    // $("#news-app-filters").addClass('hidden');
    // $(".rss-feed").addClass('hidden');

    this.setUpPagination();
    this.renderPageOfData();

  }

  setUpWithFilters() {
    let catreq = this.getCategories();
    let typereq = this.getNewsTypes();

    $.when(catreq, typereq).then(() => {


      let context = {categories: this.categoryList, types: this.newsTypeList};
      let html = com_newsroom.templates.filters(context);
      $(".app-filters").html(html);


      $('#filter-date').daterangepicker(
        {
          ranges: {
            'Today': [moment(), moment()],
            'Last 7 Days': [moment().subtract(7, 'days'), moment()],
            'Last 30 Days': [moment().subtract(30, 'days'), moment()],
            'All Data': [moment('20180101', 'YYYYMMDD'), moment()]
          },
          locale: {
            format: 'MMM DD, YYYY',
            separator: " - "
          },
          maxDate: moment(),
          startDate: this.default_range_startDate,
          endDate: this.default_range_endDate
        }
      );
      let start = $("input[name='daterangepicker_start']");
      let end = $("input[name='daterangepicker_end']");
      start.attr('id', 'daterangepicker_start');
      start.append("<label for='daterangepicker_start' class='sr-only'>start date</label>");
      end.attr('id', 'daterangepicker_end');
      end.append("<label for='daterangepicker_end' class='sr-only'>end date</label>");


      this.setUpPagination();

      this.setUpEvents();

      this.setUpRouter();
      if ($(document).width() < 768) {
        this.collapseFilterPanel();
      }
    })
  }


  setUpApp() {
    if (!this.config.show_filters) {
      this.setUpFilterless()
    } else {
      this.setUpWithFilters()
    }
  }

  setConfigurationState(hashVals) {

    if (this.config.initial_topic) {
      let option = this.config.initial_topic;
      hashVals.topic = $('#filter-topic').find('option').filter(function () {
        return $(this).html() === option;
      }).val();


    }
    if (this.config.initial_type) {
      let option = this.config.initial_type;
      hashVals.type = $('#filter-type').find('option').filter(function () {
        return $(this).html() === option;
      }).val();

    }
    /*
    //Use push state to avoid triggering a hash change event trigger
    if (history.pushState) {
      history.pushState(null, null, "#" + rewindJSUtils.getHashValueStr(hashVals));
    } else {
      rewindJSUtils.putHashValues(hashVals);
    }
  */
    this.app_router.navigate(rewindJSUtils.getHashValueStr(hashVals), {trigger: false, replace: true});
  }

  resetFilters() {
    let hashVals = rewindJSUtils.getHashValues();
    if (!this.initialized && $.isEmptyObject(hashVals)) {
      this.setConfigurationState(hashVals);
      this.initialized = true;
    }

    let $search = $("#com_news_search");
    let $topic_filter = $("#filter-topic");
    let $type_filter = $("#filter-type");
    let $date_range = $('#filter-date');
    if ($topic_filter.val() === "" && hashVals.topic) {
      $topic_filter.val(hashVals.topic);
    }
    if (!hashVals.topic) {
      $topic_filter.val("");
    }

    if ($type_filter.val() === "" && hashVals.type) {
      $type_filter.val(hashVals.type);
    }
    if (!hashVals.type) {
      $type_filter.val("");
    }
    if ($search.val() === "" && hashVals.search) {
      $search.val(hashVals.search);
      this.setSearchClearer(hashVals.search);
    }
    if (!hashVals.search) {
      $search.val("");
      this.setSearchClearer("");
    }
    let start = $date_range.data('daterangepicker').startDate.format("YYYY-MM-DD");
    let end = $date_range.data('daterangepicker').endDate.format("YYYY-MM-DD");
    if (hashVals.dates) {
      let hstart = hashVals.dates.split("_")[0];
      let hend = hashVals.dates.split("_")[1];
      if (hstart !== start || hend !== end) {
        $date_range.data('daterangepicker').setStartDate(moment(hstart, "YYYY-MM-DD"));
        $date_range.data('daterangepicker').setEndDate(moment(hend, "YYYY-MM-DD"));
      }
      //console.log(start + " _ " + end + " _ " + start + " _ " + hend);
    } else {
      $date_range.data('daterangepicker').setStartDate(this.default_range_startDate);
      $date_range.data('daterangepicker').setEndDate(this.default_range_endDate);
    }
    this.setAppliedFilters(hashVals);
    // if (this.config.max_pages > 1) {
    //   $('#numberPageSelection').bootpag({page: 1})
    //     .find("li a").each(function (i, item) {
    //      $(item).html("<span>" + item.innerHTML + "</span>");
    //   });
    // }
    this.current_page = 1;
    //this.renderPage();
  }

  /* at this point were using the router to simply push state */
  setUpRouter() {
    let AppRouter = Backbone.Router.extend({
      routes: {
        "*actions": "defaultRoute"
      },
      execute: function (callback, args, name) {
        //console.log("router path:" + name);
      }
    });
    // Initiate the router
    this.app_router = new AppRouter;


    this.app_router.on('route:defaultRoute', (actions) => {
      console.log("default route:", actions);

      this.resetFilters();
      this.renderPageOfData().then(() => {
        console.log("rendered route:", actions);
      });

    });
    // Start Backbone history a necessary step for bookmarkable URL's
    Backbone.history.start();

  }


  $(selector) {
    return this.$container.find(selector);
  }
}
